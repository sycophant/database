var db = require("../db");

var Page = db.model("Page", {
	pageID: String,
	pageName: String,
	datecreated: { type: Date, default: Date.now },
});

module.exports = Page;