// models/image.js
var db = require("../db");

var Image = db.model("Image", {
    filename:  { type: String, required: true },
    photo_name: { type: String, required: true },
    path: { type: String, required: true },
    owner: String,
    album: String,
    description: String,
    fStop: String,
    sSpeed: String,
    iso: String,
    focalLength: String,
    cameraType: String,
    upload_date: { type: Date, default: Date.now }
});

module.exports = Image;