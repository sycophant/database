// models/user.js
var db = require("../db");

// Create a model from the schema
var User = db.model("User", {
    username:  { type: String, required: true },
    password:  { type: String, required: true },
    fullname:  { type: String, required: true },
    datecreated: { type: Date, default: Date.now },
});

module.exports = User;