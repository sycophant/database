// Server initialization

const express = require('express');
const bodyParser = require('body-parser');
var User = require("./models/user");
var cors = require('cors');

var app = express();
app.use(cors());
const PORT = 3000;

app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: true }));

var router = express.Router();
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: false }));
router.use("/api/images",require("./api/images"));
router.use("/api", require("./api/users"));
router.use("/api/page", require("./api/pages"));

app.use(router);

app.listen(PORT, () => {
    console.log('Listening at ' + PORT );
});