// api/users.js
var jwt = require("jwt-simple");
var User = require("../models/user");
var conn = require("../sqldb");
var router = require("express").Router();
var bcrypt = require("bcrypt-nodejs");
var bodyParser = require("body-parser");
var fs = require("fs");
var crypto = require("crypto");
var config = require("../config/config.json");


// For encoding/decoding JWT
var debug = false;
var secret = config.secret;

router.use(bodyParser.json());

// Add a new user to the database
router.post("/users", function(req, res) {
   var queryUser = 'SELECT username, password FROM users WHERE username = ?';
   conn.query(queryUser, req.body.username, function(err, rows) {

      if (err) throw err;

      user = {};

      rows.forEach(function(row){
         user.username = row.username;
         user.password = row.password;
      });

      if(user.username != null){
         if (debug){
            console.log("Username already exists " + req.body.username);
         };
         res.sendStatus(409);
      }

      else {
         if (debug) {
            console.log(req.body.username + ", " + req.body.password + ", " + req.body.fullname);
         };
         // Create a hash for the submitted password
         bcrypt.hash(req.body.password, null, null, function(err, hash) {
            var newuser = {
               username: req.body.username,
               password: hash,
               fullname: req.body.fullname,
            };

            if (debug) console.log('New user ' + newuser.username);
            var usrDir = crypto.createHash('sha256').update(newuser.username).digest("hex");

            if (debug) console.log('Directory ' + usrDir + ' for user ' + newuser.username);
            var newDir = "public/images/" + usrDir;

            fs.mkdir(newDir, function(err){
               if (err) {
                  if (debug) {
                     console.log('new directory not made');
                  };
                  return res.status(400).json({error: newuser.username + " directory not made."});
               };
               if (debug) console.log("directory made.");
            });

            var querySave = 'insert into users set ?';
            var query = conn.query(querySave, newuser, function(err, result, fields) {
               if (err) {
                  return res.status(500).json({error: "server error."});
               }
               res.status(201).json({success: "user made."});
            });
         });
      }
   });
});

// Sends a token when given valid username/password
router.post("/auth", function(req, res) {

   // Get user from the database
   var queryUser = 'SELECT username, password FROM users WHERE username = ?';
   conn.query(queryUser, req.body.username, function(err, rows) {

      if (err) throw err;

      user = {};

      rows.forEach(function(row){
         user.username = row.username;
         user.password = row.password;
      });
 
      if (!user.username) {
         if (debug){
            console.log("Duplicate user. " + req.body.username);
         };
         // Username not in the database
         res.status(401).json({ error: "Bad username or password."});
      }
      else {
         // Does given password hash match the database password hash?
         bcrypt.compare(req.body.password, user.password, function(err, valid) {
            if (err) {
               res.status(400).json({ error: err});
            }
            else if (valid) {
               // Send back a token that contains the user's username
               var token = jwt.encode({username: user.username}, secret);
               return res.json({token: token});
            }
            else {
               res.status(401).json({ error: "Bad username or password."});
            }
         });
      };
   });
});

module.exports = router;