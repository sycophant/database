// api/images.js
var jwt = require("jwt-simple");
var User = require("../models/user");
var Image = require("../models/image");
var conn = require("../sqldb");
var router = require("express").Router();
var multer = require('multer');
var path = require('path');
var fs = require("fs");
var crypto = require("crypto");
var config = require("../config/config.json");

//conf stuff
var secret = config.secret;
var debug = false;

// set storage
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'upload')
  },
  filename: function (req, file, cb) {
    cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
  }
})

// set upload object to store pictures to correct location
var upload = multer({ storage: storage })

// Get list of all images in the database
router.get("/", function(req, res) {
    var token = req.query.u;
    var decoded;
    try {
        if (debug) console.log("token decoding image get");
        decoded = jwt.decode(token, secret);
    } catch(e) {
        if (debug) console.log("token did not decode");
        return res.status(401).json({error: "Invalid JWT"});
    }

    var queryUser = 'SELECT username FROM users WHERE username = ?';
    conn.query(queryUser, decoded.username, function(err, rows) {
        if (err) {
            if (debug) console.log("Searching for user");
            console.log(err);
            return res.status(500).json({error: "server error."});
        };

        user = {};

        rows.forEach(function(row){
            user.username = row.username;
        });

        if (debug) console.log("Retrieving images for user " + user.username);

        if (user.username != null) {
            var queryImages = 'SELECT filename, path FROM images WHERE owner = ?';
            conn.query(queryImages, user.username, function(err, rows) {
                images = [];
                rows.forEach(function(row){
                    image = {}
                    image.filename = row.filename;
                    image.path = row.path;
                    images.push(image);
                });

                if (err) {
                    res.status(500).json({error: "server error"});
                } else {
                     if (debug) console.log(images);
                    res.status(201).json(images);
                }
            });
        }
        else res.status(404).json({error: "User not found"});
    });
});

//save one image to the database
router.post('/', upload.single('photo'), function(req, res) {

    if(req.file) console.log("File: " + req.body.photoName + " saved on.");

    else {
        if (debug) console.log({error: "Could not store image a"});
        return res.status(401).json({error: "Could not store image b"});
    }

    if (!req.headers["x-auth"]){
        if (debug) console.log("no xauth header");
        return res.status(401).json({error: "no xauth header"});
    }

    var token = req.headers["x-auth"];
    if (debug) console.log("token: " + token);

    var decoded;
    try {
        if (debug) console.log("token decoding image post");
        decoded = jwt.decode(token, secret);
    } catch(e) {
        if (debug) console.log("token did not decode");
        return res.status(401).json({error: "Invalid JWT"});
    }

    var queryUser = 'SELECT username FROM users WHERE username = ?';
    conn.query(queryUser, decoded.username, function(err, rows) {
        if (err) {
            if (debug) console.log("Invalid JWT: user not found.");
            return res.status(400).json({error: "Invalid JWT"});
        }

        user = {};
        rows.forEach(function(row){
            user.username = row.username;
        });

        if (debug) console.log ("Generating image subdir" + user.username);

        var userSdir = crypto.createHash('sha256').update(user.username).digest("hex");
        if (debug) console.log("Generating image subdir for " + user.username);

        var src = "upload/" + req.file.filename;
        var dest = "public/images/" + userSdir + "/" + req.file.filename;

        fs.copyFile(src, dest, function(err) {
            if (err) {
                if (debug) console.log("Image copy from upload to " + dest + " failed");
                return res.status(507).json({error: "Image upload failed", errMsg: err});
            }
        });

        if (debug) console.log("Uploaded file successfully copied to image subdir");

        fs.unlink(src, function(err) {
            if (err) console.log("File " + src + " was not deleted");
            console.log("File " + src + " was deleted");
        })

        // make a new Image object from the input data
        var img = {
            filename: req.file.filename,
            photo_name: req.body.photoName,
            path: userSdir,
            owner: user.username,
            album: req.body.album,
            description: req.body.descr,
            fStop: req.body.fStop,
            sSpeed: req.body.sSpeed,
            iso: req.body.iso,
            focalLength: req.body.focalLength,
            cameraType: req.body.cameraType,
            upload_date: new Date(),
        };

        var querySave = 'insert into images set ?';

        // save the image to the database
        conn.query(querySave, img, function(err, result, fields) {
            if (err) {
                if (debug) console.log("Upload to db failed");
                res.status(400).send(err);
            } else {
                if (debug) console.log("Uploaded to db");
                res.sendStatus(201);
            }
        });
    });
});


module.exports = router;