//api/pages.js
var jwt = require("jwt-simple");
var Page = require("../models/page");
var User = require("../models/user");
var router = require("express").Router();
var conn = require("../sqldb");
var multer = require('multer');
var path = require('path');
var fs = require("fs");
var crypto = require("crypto");
var config = require("../config/config.json");


var secret = config.secret;
var debug = false;

router.get('/', function(req, res){
	if (debug) console.log("entered api/page");

	if (!req.headers["x-auth"]) {
		if (debug) console.log ("no auth header");
		return res.status(401).json({error: "Missing x-auth header"});
	}

	var token = req.headers["x-auth"];
    if (debug) console.log("token: " + token);

    var decoded;
    try {
        if (debug) console.log("token decoding page get");
        decoded = jwt.decode(token, secret);
    } catch(e) {
        if (debug) console.log("token did not decode pages");
        return res.status(401).json({error: "Invalid JWT"});
    }
    var usrn = decoded.username;

    var queryUser = 'SELECT username FROM users WHERE username = ?';
    conn.query(queryUser, decoded.username, function(err, rows) {
        if (err) {
            if (debug) console.log("server error");
            return res.status(500).json({error: "server error"});
        }

        if (debug) console.log ("Getting page " + req.query.pageid + " for " + user.username);

        user = {};

        rows.forEach(function(row){
            user.username = row.pageID;
        });

    	if (user.username != null) {
            var queryPage = 'SELECT pageID, pageName FROM pages WHERE pageID = ?';
            conn.query(queryPage, req.query.pageid, function(err, rows) {
    			if (err) {
    				if (debug) console.log("Page " + req.query.pageID + " not found");
    				return res.status(500).json({error: "server error"});
    			}

                page = {};

                rows.forEach(function(row){
                    page.pageID = row.pageID;
                    page.pageName = row.pageName;
                });

    			if (page) {
    				var pathPage = path.resolve("pages/" + page.pageName);
    				return res.status(200).sendFile(pathPage);
    			}
    			else return res.status(404).json({error: "Page not found"});
    		});
    	}

    	else res.status(401).json({error: "Not authorized"});
    });
});

module.exports = router;