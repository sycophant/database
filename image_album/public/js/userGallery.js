// load the image information from the database
$.get(('api/images?u=' + localStorage.getItem('token')), (data) => {
    let html = '';
    //build a card for each image
    for (var i = 0; i < data.length; i++) {
        html += '<div class="item selfie col-lg-3 col-md-4 col-6 col-sm">\n ' +
            '<a href="images/' + data[i].path + "/" + data[i].filename + '" class="fancylight popup-btn" data-fancybox-group="light">\n' +
            '<img class="img-fluid" src="images/' + data[i].path + "/" + data[i].filename + '" alt=""> \n' +
            '</a>\n' + '</div>\n';
    };

    $('#imageArea').html(html);

    $('.portfolio-item').isotope({
        itemSelector: '.item',
        layoutMode: 'fitRows'
    });

    var popUp = $('.popup-btn');
    popUp.magnificPopup({
        type: 'image',
        gallery: {
            enabled: true
        }
    });

    $('#logout').click(function logout() {
    localStorage.removeItem('token');
    location.href = "index.html";
});

    $("#uploadform").submit(function(e) {

        e.preventDefault();
        var form = $(this);
        var formData = new FormData($('form')[0]);;

        $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: 'api/images',
            data: formData,
            headers: {
                "X-Auth": window.localStorage.getItem("token")
            },
            contentType: false,
            processData: false,
            statusCode: {
                201: function() {
                    location.reload();
                }
            },
        });
    });
});