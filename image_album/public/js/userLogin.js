$(function() {
    //jsx functionality
    class LoginForm extends React.Component {
      constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.inputemail = React.createRef();
        this.inputpwd = React.createRef();
      }

      handleSubmit(event) {
        handleLogin(this.inputemail.current.value, this.inputpwd.current.value)
        event.preventDefault();
      }

      render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <div className="form-group">
                    <label>
                      Your email:
                      <input name="username" id="username" className="form-control" placeholder="Email" type="email" ref={this.inputemail} />
                    </label>
                </div>
                <div className="form-group">
                    <label>
                        Your password:
                        <input type="password" className="form-control" placeholder="******" name="password" id="password" ref={this.inputpwd} />
                    </label>
                </div>
                <div className="form-group">
                    <input type="submit" className="btn btn-primary btn-block" name="login" id="login" value="Log In"/>
                </div>
            </form>
        );
      }
    }

    ReactDOM.render(
      <LoginForm />,
      document.getElementById('loginForm')
    );
    //calls to server
    function handleLogin(username, password) {

        var login = {
            username: username,
            password: password,
        };

        $.ajax({
                url: 'api/auth',
                type: 'POST',
                data: login,
                statusCode: {
                    401: function(resObj, textStatus, jqXHR) {
                        console.log("authentication Error");
                    }
                }
            })
            .fail(function(jqXHR) {
                if (jqXHR.status != 401 || jqXHR.status != 404) alert('Error' + jqXHR.status + ',' + jqXHR.statusText);
            })
            .done(function(data) {
                localStorage.setItem("token", data.token);
                loadGallery();
                location.href = 'gallery.html';
            });
    };
    //stub for adapting to react
    function loadGallery(){

        var pagen = '666';
        var token = localStorage.getItem('token');

        $.ajax({
            url: 'api/page?pageid='+pagen,
            type: 'GET',
            headers: {"X-Auth": token},
            data: localStorage.getItem('logon'),
            statusCode: {
                401: function(resObj, textStatus, jqXHR) {
                    alert('Not authorized.');
                },
                404: function(resObj, textStatus, jqXHR) {
                    alert('Page not found.');
                }
            }
        })
        .fail(function(jqXHR) {
            if (jqXHR.status != 409) alert('Error' + jqXHR.status + ',' + jqXHR.statusText);
        })
        .done(function(data) {
            data = data.trim();
            console.log(data);
        });
    };
});