$(function() {
    
    $("#create").click(function create() {
        var username = $("#username").val();
        var password = $("#password").val();
        var fname = $("#fname").val();
        var lname = $("#lname").val();
        var requestData = {
            username: username,
            password: password,
            fullname: lname + ', ' + fname,
        };

        $.ajax({
            url : "/api/users",
            type: "POST",
            data : requestData,
            statusCode: {
                201: function(){
                    location.href = "index.html";
                }
            },
        });
    });
});